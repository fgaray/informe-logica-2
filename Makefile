all: informe.pdf

informe.pdf: informe.tex memoria.cls bibliografia.bib
	pdflatex -shell-escape informe.tex
	biber informe 
	pdflatex -shell-escape informe.tex
	biber informe 
	pdflatex -shell-escape informe.tex

clear:
	rm informe.pdf informe.bbl informe.aux informe.bcf informe.blg informe.lof informe.log informe.lot  informe.run.xml  informe.toc

ver: informe.pdf
	apvlv informe.pdf
